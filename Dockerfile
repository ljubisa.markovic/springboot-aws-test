FROM centos
RUN yum install java-11-openjdk-devel -y
COPY /target/sherlock-0.0.1-SNAPSHOT.jar sherlock.jar
RUN sh -c 'touch /sherlock.jar'
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/sherlock.jar"]
