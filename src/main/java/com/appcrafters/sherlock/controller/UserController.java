package com.appcrafters.sherlock.controller;


import com.appcrafters.sherlock.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/uuid/")
public class UserController {

    @GetMapping
    public ResponseEntity<User> getUUID() {
        User user = new User();
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
}