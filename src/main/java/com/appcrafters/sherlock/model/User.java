package com.appcrafters.sherlock.model;

import lombok.*;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

@EqualsAndHashCode
@Getter
public class User implements Comparable<User> {

    private UUID id;

    public User() {
        id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public int compareTo(User user) {
        requireNonNull(user);
        return new CompareToBuilder().append(id, user.toString()).toComparison();
    }

}
